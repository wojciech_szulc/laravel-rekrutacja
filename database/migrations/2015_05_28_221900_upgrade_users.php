<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('permissions', function($table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('permission_user', function($table) {
            $table->increments('id');
            $table->integer('permission_id');
            $table->integer('user_id');
            $table->foreign('permission_id')
                ->references('id')->on('permissions')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('userss')
                ->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('permission_user', function($t) {
            $t->dropForeign('user_id');
            $t->dropForeign('permission_id');
        });
        Schema::table('users', function($t) {
            $t->dropColumn('first_name');
            $t->dropColumn('last_name');
        });
		Schema::drop('permission_user');
		Schema::drop('permissions');

	}

}
