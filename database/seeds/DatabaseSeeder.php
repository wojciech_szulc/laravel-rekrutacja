<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        DB::table('users')->delete();
        DB::table('permissions')->delete();

        $admin = new App\User;
        $admin->email = 'admin@example.com';
        $admin->password = Hash::make('pass');
        $admin->first_name = 'Wojciech';
        $admin->last_name = 'Szulc';
        $admin->save();

        $user = new App\User;
        $user->email = 'user@example.com';
        $user->password = Hash::make('pass');
        $user->save();

        $can_edit = new App\Permission;
        $can_edit->name = 'can_edit';
        $can_edit->save();

        $can_see = new App\Permission;
        $can_see->name = 'can_see';
        $can_see->save();

        $can_delete = new App\Permission;
        $can_delete->name = 'can_delete';
        $can_delete->save();

        $can_create = new App\Permission;
        $can_create->name = 'can_create';
        $can_create->save();

        $admin->permissions()->saveMany([$can_edit, $can_create, $can_delete, $can_see]);
        $user->permissions()->save($can_see);

	}

}
