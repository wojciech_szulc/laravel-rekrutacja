@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Użytkownicy</span>
                        <div class="pull-right"><a href="{{ URL::to('users/create') }}" class="btn btn-xs btn-primary">Dodaj</a> </div>
                    </div>

                    <div class="panel-body">
                        <table class="table table-condensed table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 25%">Imię</th>
                            <th style="width: 25%">Nazwisko</th>
                            <th style="width: 30%">E-mail</th>
                            <th style="width: 15%">Uprawnień</th>
                            <th style="width: 5%"></th>
                        </tr>
                        @foreach($users as $user)
                        <tr id="user-{{{ $user->id }}}">
                            <td>{{{ $user->first_name }}}</td>
                            <td>{{{ $user->last_name }}}</td>
                            <td>{{{ $user->email }}}</td>
                            <td>
                                <div class="popover-holder" data-toggle="tooltip" data-placement="top" title="@foreach($user->permissions()->get() as $p) {{{ $p->name }}} @endforeach">{{{ $user->permissions()->count() }}}</div>
                            </td>
                            <td>
                                @if(Auth::user()->permissions()->where('name','can_edit')->first())
                                    <a href="{{ URL::to('users/edit/'.$user->id) }}"><span class="glyphicon glyphicon-edit"></span> </a>
                                @endif
                                @if(Auth::user()->permissions()->where('name','can_delete')->first())
                                    <a href="" class="ajaxDelete" data-id="{{ $user->id }}"><span class="glyphicon glyphicon-trash"></span> </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jquery')
    <script>
        $(function(){
            $('.ajaxDelete').on('click', function() {
                var userid = $(this).attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                $.ajax({
                    url: '{{{ URL::to('users/delete/') }}}/'+userid,
                    method: 'POST',
                    success: function(d,s,jq) {
                        if (d == 'deleted') {
                            $('#user-'+userid).remove();
                        }
                    }
                });
            });
        });
    </script>
@endsection