@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span>Dodawanie nowego użytkownika</span>
                        <div class="pull-right"><a href="{{ URL::to('users/list') }}" class="btn btn-xs btn-danger">Wróć</a> </div>
                    </div>

                    <div class="panel-body">
                        <form method="post" action="{{ URL::to('users/create') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Imię</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Nazwisko</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">E-mail</label>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group @if($errors->first('password'))has-errors @endif">
                                <label class="col-md-3 control-label">Nowe hasło</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="password">
                                </div>
                                @if($errors->first('password')) {{{ $errors->first('password') }}} @endif
                            </div>

                            <div class="form-group @if($errors->first('confirm'))has-errors @endif">
                                <label class="col-md-3 control-label">Powtórz nowe hasło</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="confirm_password">
                                </div>
                                @if($errors->first('confirm')) {{{ $errors->first('confirm') }}} @endif
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Wybierz uprawnienia</label>
                                <div class="col-md-9">
                                    @foreach(\App\Permission::get() as $p)
                                        <input type="checkbox" name="permissions[]" value="{{{ $p->id }}}"> {{{ $p->name }}} &nbsp;
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success">Utwórz</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

