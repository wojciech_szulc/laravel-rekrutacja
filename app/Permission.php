<?php namespace App;
/**
 * Created by PhpStorm.
 * User: Forien
 * Date: 2015-05-29
 * Time: 00:43
 */

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
