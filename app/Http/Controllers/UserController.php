<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: Forien
 * Date: 2015-05-29
 * Time: 01:17
 */

use App\User;
use App\Permission;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Lista użytkowników
     * 
     * @return \Illuminate\View\View
     */
    public function getList()
    {
        if (!Auth::user()->permissions()->where('name','can_see')->first()) {
            Redirect::to('/');
        }
        $users = User::all();
        return view('users.list', ['users' => $users]);
    }

    /**
     * Edytowanie użytkowników
     * 
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        if (!Auth::user()->permissions()->where('name','can_edit')->first())
            Redirect::to('/');
        $user = User::find($id);
        if (!$user)
            abort(404);
        return view('users.edit', ['user' => $user]);
    }

    public function postEdit($id)
    {
        if (!Auth::user()->permissions()->where('name','can_edit')->first())
            Redirect::to('/');
        $user = User::find($id);
        if (!$user)
            abort(404);

        $password = Input::get('password');
        $confirm = Input::get('confirm_password');
        $validator = Validator::make(
            [
                'password' => $password,
                'confirm' => $confirm
            ],
            [
                'password' => 'required_with:confirm',
                'confirm' => 'required_with:password|same:password'
            ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        } elseif (!empty($password)) {
            $user->password = Hash::make(Input::get('password'));
        }
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->email = Input::get('email');
        $user->save();

        $user->permissions()->detach();

        if (Input::has('permissions'))
            foreach (Input::get('permissions') as $p) {
                $user->permissions()->save(Permission::find($p));
            }

        return Redirect::to('users/list');
    }

    /**
     * Tworzenie użytkowników
     * 
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        if (!Auth::user()->permissions()->where('name','can_create')->first())
            Redirect::to('/');
        return view('users.create');
    }    
    
    public function postCreate()
    {
        if (!Auth::user()->permissions()->where('name','can_create')->first())
            Redirect::to('/');

        $password = Input::get('password');
        $confirm = Input::get('confirm_password');
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $email = Input::get('email');

        $validator = Validator::make(
            [
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'password' => $password,
                'confirm' => $confirm
            ],
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'confirm' => 'required|same:password'
            ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $user = new User;

        $user->password = Hash::make($password);
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $email;

        $user->save();

        if (Input::has('permissions'))
            foreach (Input::get('permissions') as $p) {
                $user->permissions()->save(Permission::find($p));
            }

        return Redirect::to('users/list');
    }

    /**
     * Usuwanie użytkowników (ajax via jQuery)
     * 
     * @param $id
     * @return string
     */
    public function postDelete($id)
    {
        if (!Auth::user()->permissions()->where('name','can_delete')->first())
            Redirect::to('/');
        if (Request::ajax()) {
            $user = User::find($id);
            if (!$user)
                abort(404);
            $user->delete();
            return 'deleted';
        }
    }
}