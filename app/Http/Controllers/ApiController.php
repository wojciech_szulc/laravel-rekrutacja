<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: Forien
 * Date: 2015-05-29
 * Time: 03:21
 */

use App\Permission;
use App\User;

class ApiController extends Controller
{
    public function getUsers()
    {
        $users = User::all()->toArray();

        foreach ($users as &$user) {
            $id = $user['id'];
            $zezwolenia = User::find($id)->permissions()->get();
            foreach ($zezwolenia as $z) {
                $user['permissions'][] = $z->name;
            }
        }
        return response()->json($users);
    }
}