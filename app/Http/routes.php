<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function()
{
    ##  Lista użytkowników
    Route::get('/users/list', 'UserController@getList');

    ##  Edycja użytkowników
    Route::get('/users/edit/{id}', 'UserController@getEdit')->where('id', '[0-9]+');
    Route::post('/users/edit/{id}', 'UserController@postEdit')->where('id', '[0-9]+');

    ##  Dodawanie użytkowników
    Route::get('/users/create', 'UserController@getCreate');
    Route::post('/users/create', 'UserController@postCreate');

    ##  Usuwanie użytkowników
    Route::post('/users/delete/{id}', 'UserController@postDelete')->where('id', '[0-9]+');

});

##  API pobierające dane użytkowników
Route::get('/api/users', 'ApiController@getUsers');
